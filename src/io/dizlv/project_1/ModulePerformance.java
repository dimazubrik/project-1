package io.dizlv.project_1;

/**
 * @author Dmitrijs Zubriks.
 */

/**
 * Hold 3 module performance constants to make main code more readable.
 */
public enum ModulePerformance {
    PASS, COMPENSATABLE_FAIL, FAIL;
}
