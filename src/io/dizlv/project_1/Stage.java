package io.dizlv.project_1;

/**
 * @author Dmitrijs Zubriks
 */

import java.util.ArrayList;

/**
 * Holds information about whole Stage.
 */
public class Stage {

    private ArrayList<Module> modules = new ArrayList<>();

    /**
     * Setter method. Adds new module to the modules list.
     * @param module module object.
     * @see io.dizlv.project_1.Module
     */
    public void addModule(Module module) {
        modules.add(module);
    }

    /**
     * Getter method. Returns student module with particular index.
     * @param index module position in ArrayList
     * @return Module object from student modules list.
     * @see io.dizlv.project_1.Module
     */
    public Module getModule(int index) {
        return modules.get(index);
    }

    /**
     * Overloaded getter function. If no index provided. Returns all modules for student.
     * @return all students modules ArrayList.
     */
    public ArrayList<Module> getModule() {
        return modules;
    }

    /**
     * Calculates whole stage modules marks average number.
     * @return average of whole module makrs.
     */
    public int getStageAverage() {
        int summary = 0;

        for (Module module : modules) {
            summary += module.computeMark();
        }

        return summary / modules.size();
    }

    /**
     * Calculates whole stage performance.
     * @return StagePerformance.
     */
    public StagePerformance getPerformance() {
        // todo: not full features implemented.
        for (Module module : modules) {
            ModulePerformance modulePerformance = module.getPerformance(module.computeMark());

            if (modulePerformance != ModulePerformance.PASS) {
                return StagePerformance.FAIL;
            } else {
                return StagePerformance.PASS;
            }
        }

        // if not modules provided returns null.
        return null;
    }

}
