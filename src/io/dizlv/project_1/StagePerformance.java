package io.dizlv.project_1;

/**
 * @author Dmitrijs Zubriks
 */

/**
 * Hold 3 stage performance constants to make main code more readable.
 */
public enum StagePerformance {
    PASS, PASS_BY_COMPENSATION, FAIL;
}
