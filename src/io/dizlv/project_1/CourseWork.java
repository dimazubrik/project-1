package io.dizlv.project_1;

/**
 * @author Dmitrijs Zubriks
 */

/**
 * Holds information about Course work.
 */
public class CourseWork extends Component {

    /**
     * Initialize mark with specific value.
     * @param mark int mark score.
     */
    public CourseWork(int mark) {
        super(mark);
    }
}
