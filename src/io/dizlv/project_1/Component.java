package io.dizlv.project_1;

/**
 * @author Dmitrjis Zubriks.
 */


public class Component {

    private int mark;

    /**
     * Initialize mark with value.
     * @param mark int score for Component.
     */
    public Component(int mark) {
        this.mark = mark;
    }

    public int getMark() {
        return mark;
    }
}
