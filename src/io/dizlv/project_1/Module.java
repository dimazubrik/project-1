package io.dizlv.project_1;

/**
 * @author Dmitrijs Zubriks.
 */

/**
 * Module representation.
 */
public class Module {

    private Exam exam;
    private CourseWork courseWork;

    /**
     * Initialize module with Exam mark and CourseWork mark.
     * @param exam Mark object with score for exam.
     * @param courseWork Mark object with score for course work.
     */
    public Module(Exam exam, CourseWork courseWork) {
        this.exam = exam;
        this.courseWork = courseWork;
    }

    /**
     * Computes mark relying on course work mark and exam mark and course work weighting.
     * If the exam mark and coursework mark are greater than or equal to 35 then
     * the returned mark is the computed module mark
     * However, if either (or both) the exam or coursework mark is less than 35 then the returned
     * mark is the minimum(35, computed module mark).
     * @return computed mark.
     */
    public int computeMark() {
        // todo: wtf?!
        int coursework_weighting = 50;

        int courseWorkMark = courseWork.getMark();
        int examMark = exam.getMark();

        int mark = ((courseWorkMark * coursework_weighting) +
                    (examMark * (100 - coursework_weighting))) / 100;

        if (examMark >= 35 && courseWorkMark >= 35) {
            return mark;
        } else {
            return Math.min(35, mark);
        }

    }

    /**
     * Based on module_mark (commonly calculated by computeMark() method) returns ModulePerfromance.
     * @param module_mark int module mark (commonly calculated by computeMark() method).
     * @return ModulePerformance state.
     */
    public ModulePerformance getPerformance(int module_mark) {
        if (module_mark >= 40) {
            return ModulePerformance.PASS;
        } else if (module_mark < 40 && module_mark >= 35) {
            return ModulePerformance.COMPENSATABLE_FAIL;
        } else {
            return ModulePerformance.FAIL;
        }
    }
}