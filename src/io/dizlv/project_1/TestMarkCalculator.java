package io.dizlv.project_1;

public class TestMarkCalculator {

    public static void main(String[] args) {
        MarkCalculator markCalculator = new MarkCalculator();

        int[] marks = {25, 20};

        int computed_marks = markCalculator.computeMarks(marks);

        System.out.println(computed_marks);
        System.out.println(markCalculator.stage.getModule(0).getPerformance(computed_marks));
        System.out.println(markCalculator.stage.getPerformance());
    }

}
