package io.dizlv.project_1;

/**
 * @author Dmitrijs Zubriks
 */

import java.util.ArrayList;

/**
 * Student representation.
 */
public class Student {

    private ArrayList<Stage> stages = new ArrayList<>();

    /**
     * Add new stage to the stages list.
     * @param stage Stage object.
     * @see io.dizlv.project_1.Stage
     */
    public void addStage(Stage stage) {
        stages.add(stage);
    }

}
