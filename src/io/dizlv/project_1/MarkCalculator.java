package io.dizlv.project_1;
/**
 * @author Dmitrijs Zubriks
 */

/**
 * Entry point to collect whole API work at one place.
 */
public class MarkCalculator {

    public Student student;
    public Stage stage;

    /**
     * Initializes all test data.
     */
    public MarkCalculator() {
        // Initialize test data.
        student = new Student();
        stage = new Stage();

        student.addStage(stage);
    }

    /**
     * Computes mark for given array with exam and coursework marks.
     *
     * @param marks two-dimensional array with [0] - exam mark, [1] - course work mark. Despite of specs i would rather
     *              create two different arguments, like public void computeMarks(int examMark, int courseWorkMark);
     *
     * @return computed mark.
     */
    public int computeMarks(int[] marks) {
        Exam exam = new Exam(marks[0]);
        CourseWork courseWork = new CourseWork(marks[1]);

        Module module = new Module(exam, courseWork);

        // Since Java uses referencing object ideoma, we can proceed stage direct through stage variable.
        stage.addModule(module);

        return module.computeMark();
    }

    /**
     *
     */
    public void computeResult(int[] marks) {}

}
