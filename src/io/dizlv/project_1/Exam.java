package io.dizlv.project_1;

/**
 * @author Dmitrijs Zubriks
 */

/**
 * Holds information about Exam.
 */
public class Exam extends Component {

    /**
     * Initialize mark with specific value.
     * @param mark int mark score.
     */
    public Exam(int mark) {
        super(mark);
    }

}
